module modernc.org/atk

go 1.17

require (
	modernc.org/libc v1.11.29
	modernc.org/tcl v1.7.4
	modernc.org/tk v1.0.3
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/sys v0.0.0-20210902050250-f475640dd07b // indirect
	modernc.org/expat v0.0.1 // indirect
	modernc.org/fontconfig v0.0.1 // indirect
	modernc.org/freetype v0.0.1 // indirect
	modernc.org/gettext v0.0.0-20210309143449-7dc1ce515aac // indirect
	modernc.org/httpfs v1.0.6 // indirect
	modernc.org/mathutil v1.4.1 // indirect
	modernc.org/memory v1.0.5 // indirect
	modernc.org/x11 v1.0.3 // indirect
	modernc.org/xau v1.0.3 // indirect
	modernc.org/xcb v1.0.3 // indirect
	modernc.org/xdmcp v1.0.3 // indirect
	modernc.org/xft v0.0.1 // indirect
	modernc.org/xrender v0.0.1 // indirect
	modernc.org/z v1.2.5 // indirect
)
